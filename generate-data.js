const csvtojson = require('csvtojson');
const fs = require('fs');


csvtojson().fromFile('files/sound_recordings.csv').then((json) => {
  console.log(json);
  fs.writeFileSync('src/data/files/sound_recordings.json', JSON.stringify(json))
});

csvtojson().fromFile('files/sound_recordings_input_report.csv').then((json) => {
  console.log(json);
  fs.writeFileSync('src/data/files/sound_recordings_input_report.json', JSON.stringify(json))
});
