# Recording matcher app (recording-matcher)
Recording matcher app

## demo
https://recording-matcher.netlify.app

## Notes
- There are no real server, the simulated logic is written in `src/api`
  and it is easy to replace this logic with axios which will call a real API.
- If a real server required in this task, I will build it using nodejs/golang
  and containerize it for easy installation.


## Install the dependencies
```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
yarn dev
```

### Build the app for production
```bash
yarn build
```

### Build the app for production(PWA)
```bash
yarn build:pwa
```


## Features
- [x] SPA
- [x] PWA
- [x] Search
- [x] Pagination
- [x] Load matched/all data with checkbox
- [x] Add a new record with one click
- [x] Table loading state
- [x] Action loading state
- [x] Simulate backend loading
- [x] Matches count
- [x] Matched list
- [x] Back button
- [x] Dashboard with loading states
- [ ] Server side search(not needed now because there are no server)
- [ ] Server side pagination(not needed now because there are no server)
- [ ] Unit tests


## Why this layout
- Left drawer for easy navigation
- Match recording in a new page
  because maybe the matched list is very large
  and to make the page focused to match the recording.

## To improve user experience
- Add quick matching control on the input recordings page
  to do a quick match instead of opening the matching page.

