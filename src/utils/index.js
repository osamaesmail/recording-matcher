export const includes = (obj, value) => {
  return Object.keys(obj || {})
    .some((key) => {
      const keyVal = obj[key] + '';
      const comparedVal = (value[key] ?? value) + '';
      return keyVal !== '' && comparedVal !== ''
        && keyVal.toLocaleLowerCase()?.includes(comparedVal.toLocaleLowerCase())
    })
}
