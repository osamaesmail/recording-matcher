import {ref} from 'vue';

export const useToggle = (val) => {
  const toggle = ref(val);

  return [
    toggle,
    () => toggle.value = !toggle.value,
  ];
}
