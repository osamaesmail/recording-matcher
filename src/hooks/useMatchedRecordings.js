import {useStore} from 'vuex';
import {computed, onMounted, ref, watch} from 'vue';


export const useMatchedRecordings = (isSelected, id) => {
  const store = useStore();
  const isAllRecordings = ref(isSelected);
  const matchedRecords = computed(
    () => isAllRecordings.value ? store.getters['recording/getList'] : store.getters['recording/getListMatch']
  );

  const loadRecordings = () => {
    if (isAllRecordings.value) {
      store.dispatch('recording/loadList');
      return;
    }
    store.dispatch('recording/loadListMatch', id);
  }

  watch(isAllRecordings, () => loadRecordings());

  onMounted(() => loadRecordings())

  return [
    isAllRecordings,
    matchedRecords,
  ]
}
