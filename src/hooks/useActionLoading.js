import {ref} from 'vue';

export const useActionLoading = (callback) => {
  const loading = ref(false);
  const innerCallback = async (...args) => {
    loading.value = true
    await callback(args);
    loading.value = false
  }

  return [
    loading,
    innerCallback,
  ];
}
