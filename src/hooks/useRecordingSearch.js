import {ref, computed} from 'vue';
import {includes} from 'src/utils';

export const useRecordingSearch = (allRecordings) => {
  const search = ref('');
  const recordings = computed(
    () =>!search.value ? allRecordings.value : allRecordings.value?.filter((r) => includes(r, search.value))
  );

  return [
    search,
    recordings,
  ];
}
