export const items = [
  {
    toName: 'home',
    title: 'Home',
  },
  {
    toName: 'unmatched',
    title: 'Unmatched recordings',
  },
  {
    toName: 'recordings',
    title: 'All recordings',
  },
];
