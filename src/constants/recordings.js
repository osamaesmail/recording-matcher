export const unmatchedColumns = [
  {
    name: 'artist',
    field: 'artist',
    label: 'Artist',
    align: 'left',
  },
  {
    name: 'title',
    field: 'title',
    label: 'Title',
    align: 'left',
  },
  {
    name: 'isrc',
    field: 'isrc',
    label: 'ISRC',
    align: 'left',
  },
  {
    name: 'duration',
    field: 'duration',
    label: 'Duration',
    align: 'left',
  },
];

export const matchedColumns = [
  ...unmatchedColumns,
  {
    name: 'matches',
    field: 'matches',
    label: 'Matches',
    align: 'left',
    format: (v) => v?.length || 0,
  },
]
