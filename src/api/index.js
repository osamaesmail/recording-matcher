import recordings from './files/sound_recordings.json';
import recordingsInput from './files/sound_recordings_input_report.json';
import {includes} from 'src/utils';

const SIMULATION_TIME = 200;

const sleep = (ms) => {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const deleteRecordingInput = (id) => {
  recordingsInput.splice(id, 1);
}

export const getRecordings = async () => {
  await sleep(SIMULATION_TIME);
  return recordings.map((r, i) => ({...r, id: i}));
}

export const getRecordingsInput = async () => {
  await sleep(SIMULATION_TIME);
  return recordingsInput.map((r, i) => ({...r, id: i}));
}

export const getRecordingsMatched = async (id) => {
  const rows = await getRecordings();
  return rows[id]?.matches || [];
}

export const getRecordingsMatch = async (id) => {
  const rowsInput = await getRecordingsInput();
  const rows = await getRecordings();
  const inputRow = rowsInput[id] || {};
  return rows
    ?.filter((r) => includes(r, inputRow))
    ?.map((r, i) => ({...r, id: i}));
}

export const setMatched = async (id) => {
  await sleep(SIMULATION_TIME);
  recordings[id].matches = [...recordings[id].matches || [], recordingsInput[id]];
  deleteRecordingInput(id);
}

export const addRecording = async (id) => {
  await sleep(SIMULATION_TIME);
  recordings.push(recordingsInput[id]);
  deleteRecordingInput(id);
}
