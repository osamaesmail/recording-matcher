export function setList(state, val) {
  state.list = val;
}

export function setListInput(state, val) {
  state.listInput = val;
}

export function setListMatch(state, val) {
  state.listMatch = val;
}

export function setListMatched(state, val) {
  state.listMatched = val;
}
