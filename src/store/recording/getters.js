export function getList (state) {
  return state.list;
}

export function getListInput (state) {
  return state.listInput;
}

export function getListMatch(state) {
  return state.listMatch;
}

export function getListMatched(state) {
  return state.listMatched;
}

export function getCounts(state) {
  return {
    recordings: state.list?.length,
    unmatched: state.listInput?.length,
  }
}
