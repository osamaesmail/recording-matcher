import {
  getRecordings,
  getRecordingsInput,
  getRecordingsMatch,
  getRecordingsMatched,
  setMatched,
  addRecording,
} from 'src/api';

export async function loadList(context) {
  context.commit('setList', undefined);
  context.commit('setList', await getRecordings());
}

export async function loadListInput(context) {
  context.commit('setListInput', undefined);
  context.commit('setListInput', await getRecordingsInput());
}

export async function loadListMatch(context, id) {
  context.commit('setListMatch', undefined);
  context.commit('setListMatch', await getRecordingsMatch(id));
}

export async function loadListMatched(context, id) {
  context.commit('setListMatched', undefined);
  context.commit('setListMatched', await getRecordingsMatched(id));
}

export async function setMatchedInput(context, id) {
  await setMatched(id);
}

export async function addNewRecording(context, id) {
  await addRecording(id);
}
