
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {name: 'home', path: '', component: () => import('pages/Home.vue')},
      {name: 'unmatched', path: 'unmatched', component: () => import('pages/Unmatched.vue')},
      {name: 'match', path: 'match/:id', component: () => import('pages/Match.vue')},
      {name: 'matched', path: 'matches/:id', component: () => import('pages/Matched.vue')},
      {name: 'recordings', path: 'recordings', component: () => import('pages/Recordings.vue')},
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
